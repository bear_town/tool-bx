use tauri::{
    AppHandle, CustomMenuItem, Manager, SystemTray, SystemTrayEvent, SystemTrayMenu,
    SystemTrayMenuItem,
};

pub fn tray_init() -> SystemTray {
    let quit = CustomMenuItem::new("quit".to_string(), "Quit");
    let hide_or_show = CustomMenuItem::new("hide_or_show".to_string(), "Hide");
    let tray_menu = SystemTrayMenu::new()
        .add_item(quit)
        .add_native_item(SystemTrayMenuItem::Separator)
        .add_item(hide_or_show);
    SystemTray::new().with_menu(tray_menu)
}

pub fn tray_event_handler(app: &AppHandle, event: tauri::SystemTrayEvent) {
    match event {
        SystemTrayEvent::LeftClick {
            tray_id,
            position,
            size,
            ..
        } => {
            println!("system trayy received a left click");
        }
        SystemTrayEvent::RightClick {
            tray_id,
            position,
            size,
            ..
        } => {
            println!("system tray received a right click");
        }
        SystemTrayEvent::DoubleClick {
            tray_id,
            position,
            size,
            ..
        } => {
            let item_handle = app.tray_handle().get_item("hide_or_show");
            let window = app.get_window("main").unwrap();
            window.show().unwrap();
            item_handle.set_title("Hide").unwrap();
            println!("system tary received double click");
        }
        SystemTrayEvent::MenuItemClick { tray_id, id, .. } => {
            let item_handle = app.tray_handle().get_item(&id);
            match id.as_str() {
                "quit" => {
                    std::process::exit(0);
                }
                "hide_or_show" => {
                    let window = app.get_window("main").unwrap();
                    if let Result::Ok(is_visible) = window.is_visible() {
                        if is_visible {
                            window.hide().unwrap();
                            item_handle.set_title("Show").unwrap();
                        } else {
                            window.show().unwrap();
                            item_handle.set_title("Hide").unwrap();
                        }
                    }
                }
                _ => {}
            }
        }
        _ => {}
    }
}
