// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

// Learn more about Tauri commands at https://tauri.app/v1/guides/features/command

mod menu;
use menu::tray;

mod web_handler;
use web_handler::{close_splashscreen, greet};

fn main() {
    let tray = tray::tray_init();

    tauri::Builder::default()
        .system_tray(tray)
        .on_system_tray_event(tray::tray_event_handler)
        .invoke_handler(tauri::generate_handler![greet, close_splashscreen])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
