use tauri::{generate_handler, Manager, Runtime, Window};

#[tauri::command]
pub fn greet(name: &str) -> String {
    format!("Hello, {}! You've been greeted from Rust!", name)
}

#[tauri::command]
pub async fn close_splashscreen(window: Window) {
    if let Some(splashscreen) = window.get_window("splashscreen") {
        splashscreen.close().unwrap();
        println!("close the splashscreen");
    }
    window.get_window("main").unwrap().show().unwrap();
}
