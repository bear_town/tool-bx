import "./App.css";
import { RouterProvider } from "react-router-dom";
import { router } from "./routes";

function App() {
  return (
    <RouterProvider fallbackElement={<div>Loading...</div>} router={router} />
  );
}

export default App;
