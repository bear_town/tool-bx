/**
 * 判断是不是某个页面
 */

import { useLocation } from "react-router-dom";

/** 目标页面 */
export enum TargetPage {
  /** 首页 */
  Home = "/",
  /** 关于 */
  About = "/about",
  /** 设置页面 */
  Setting = "/setting",
  /** 环境页面 */
  Env = "/env",
}

/** 判断是否是目标页面 */
export function useIsTarget(target: TargetPage) {
  const { pathname } = useLocation();
  return pathname.startsWith(target);
}
