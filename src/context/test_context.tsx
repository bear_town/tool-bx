import { ReactNode, createContext } from "react";

export interface TestContextValue {
  value: number;
}

export const TestContext = createContext<TestContextValue>({ value: 1 });

export const TestContextProvider = ({ children }: { children: ReactNode }) => {
  return (
    <TestContext.Provider value={{ value: 111 }}>
      {children}
    </TestContext.Provider>
  );
};
