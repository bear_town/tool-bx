/**
 * 统一对外提供Provider和context
 */

import { ReactNode } from "react";
import { TestContextProvider } from "./test_context";
import { EnvContextProvider } from "./env_context";
export * from "./env_context";
export * from "./test_context";

/**
 * Provider统一对外提供
 */
export function Provider({ children }: { children: ReactNode }) {
  // 索引越小的，越在最外层
  const providerList = [EnvContextProvider, TestContextProvider];
  const Providers = ({ children }: { children: ReactNode }) =>
    providerList.reduceRight((LossProvider, CurProvider) => {
      return CurProvider({ children: LossProvider });
    }, children);

  return <Providers>{children}</Providers>;
}
