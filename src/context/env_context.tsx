import { getName, getTauriVersion, getVersion } from "@tauri-apps/api/app";
import {
  Arch,
  OsType,
  Platform,
  arch,
  platform,
  tempdir,
  type,
  version,
} from "@tauri-apps/api/os";
import { ReactNode, createContext, useEffect, useState } from "react";

/** APP 环境信息上下文 */
interface EnvContextValue {
  /** APP名称 */
  appName?: string;
  /** APP版本 */
  appVersion?: string;
  /**使用的tauri版本  */
  tauriVersion?: string;
  /** OS平台 */
  osPlatform?: Platform;
  /** OS 版本 */
  osVersion?: string;
  /** OS内核 */
  osKernel?: OsType;
  /** temp 临时目录path */
  tempDir?: string;
  /** 指令集 */
  arch?: Arch;
}

export const EnvContext = createContext<EnvContextValue>({});

/** 相关环境的内容上下文 */
export const EnvContextProvider = ({ children }: { children: ReactNode }) => {
  const [env, setEnv] = useState<EnvContextValue>({});
  useEffect(() => {
    (async () => {
      try {
        const [
          appName,
          appVersion,
          tauriVersion,
          osPlatform,
          osVersion,
          osKernel,
          osArch,
          tempDir,
        ] = await Promise.all([
          getName(),
          getVersion(),
          getTauriVersion(),
          platform(),
          version(),
          type(),
          arch(),
          tempdir(),
        ]);
        setEnv({
          appName,
          appVersion,
          tauriVersion,
          osPlatform,
          osVersion,
          osKernel,
          arch: osArch,
          tempDir,
        });
      } catch (error) {
        console.error("获取环境信息失败", error);
      }
    })();
  }, []);

  return <EnvContext.Provider value={env}>{children}</EnvContext.Provider>;
};
