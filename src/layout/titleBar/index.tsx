import { useState } from "react";
import styles from "./index.module.less";
import { appWindow } from "@tauri-apps/api/window";
import { TitleMenu } from "../titleMenu";

/**
 * 自定义标题栏
 * @see https://www.cnblogs.com/xxcf/articles/17332797.html#:~:text=%E7%BC%96%E8%BE%91tauri.conf.json%2C%20%E8%AE%BE%E7%BD%AEdecorations%3A%20false%E9%9A%90%E8%97%8F%E8%87%AA%E5%B7%B1%E7%9A%84%E8%BE%B9%E6%A1%86%E4%BA%86%E6%A0%8F%20%7B%20%22windows%22%3A%20%5B%20%7B,%22decorations%22%3A%20false%2C...%20%7D%20%5D%20%7D%20%E5%AE%9A%E4%B9%89%E8%87%AA%E5%B7%B1%E7%9A%84%E9%A1%B6%E6%A0%8F%20%3C%21--
 */
export function TitleBar() {
  const [isMax, setMax] = useState(false);

  return (
    <div
      data-tauri-drag-region
      onContextMenu={(e) => e.preventDefault()}
      className={styles.titleBar}
    >
      <div data-tauri-drag-region className={styles.left}>
        <img data-tauri-drag-region className={styles.logo} src="/logo.png" />
        <TitleMenu />
      </div>
      <div data-tauri-drag-region className={styles.center}>
        <span>Tool - Box</span>
      </div>
      <div data-tauri-drag-region className={styles.right}>
        <div className={styles.button} onClick={() => appWindow.minimize()}>
          <img
            src="https://api.iconify.design/mdi:window-minimize.svg"
            alt="minimize"
          />
        </div>
        <div
          className={styles.button}
          onClick={() => {
            setMax(!isMax);
            appWindow.toggleMaximize();
          }}
        >
          <img
            src={`https://api.iconify.design/mdi:${
              isMax ? "dock-window" : "window-maximize"
            }.svg`}
            alt="toggleMaximize"
          />
        </div>
        <div className={styles.button} onClick={() => appWindow.close()}>
          <img src="https://api.iconify.design/mdi:close.svg" alt="close" />
        </div>
      </div>
    </div>
  );
}
