import { TargetPage, useIsTarget } from "../hooks";

/** 是否展示Layout */
export function useShowLayout() {
  const isEnv = useIsTarget(TargetPage.Env);

  return {
    isShowFloat: !isEnv,
    isShowTitleBar: !isEnv,
  };
}
