import { message } from "antd";
import { Outlet } from "react-router-dom";
import { Provider } from "../context";
import { TitleBar } from "./titleBar";
import { LayoutFloat } from "./float";
import { useShowLayout } from "./useShowLayout";
import { useFileDrop } from "./useFileDrop";

message.config({
  top: 50,
  duration: 2,
  maxCount: 3,
});

export function MainLayout() {
  useFileDrop();
  const { isShowFloat, isShowTitleBar } = useShowLayout();

  // 需要在这里进行Provider的提供
  return (
    <>
      {/* 统一的context提供 */}
      <Provider>
        <>
          {isShowFloat && <LayoutFloat />}

          {/* 标题栏 */}
          {isShowTitleBar && <TitleBar />}

          {/* 路由渲染的地方 */}
          <div style={{ flexGrow: 1, padding: "16px" }}>
            <Outlet />
          </div>
        </>
      </Provider>
    </>
  );
}
