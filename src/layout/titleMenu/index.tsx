import { Dropdown, MenuProps } from "antd";
import { useMemo } from "react";
import styles from "./index.module.less";
import { useNavigate } from "react-router-dom";
import { WebviewWindow } from "@tauri-apps/api/window";

/**
 * 标题栏menu
 */
export function TitleMenu() {
  const navigate = useNavigate();

  const items: MenuProps["items"] = useMemo(() => {
    return [
      {
        label: (
          <div style={{ width: 180 }} onClick={() => navigate("/about")}>
            关于
          </div>
        ),
        key: "0",
      },
      {
        label: (
          <div style={{ width: 180 }} onClick={() => navigate("/404")}>
            404
          </div>
        ),
        key: "1",
      },
      {
        type: "divider",
      },
      {
        label: (
          <div style={{ width: 180 }} onClick={() => navigate("/")}>
            首页
          </div>
        ),
        key: "3",
      },
    ];
  }, []);

  // 打开设置窗口
  const handleEnv = () => {
    const webview = new WebviewWindow("env", {
      url: "/env",
      center: true,
      width: 500,
      height: 400,
      decorations: true,
      resizable: false,
      minimizable: false,
      maximizable: false,
      title: "环境信息",
      alwaysOnTop: true,
    });
    webview?.show();
  };

  return (
    <div className={styles.titleMenu}>
      <div className={styles.menuItem}>
        <Dropdown menu={{ items }} trigger={["click"]}>
          <div className={styles.menuItemLabel}>菜单</div>
        </Dropdown>
      </div>
      <div className={styles.menuItem}>
        <div className={styles.menuItemLabel}>设置</div>
      </div>
      <div className={styles.menuItem}>
        <div className={styles.menuItemLabel} onClick={handleEnv}>
          关于
        </div>
      </div>
    </div>
  );
}
