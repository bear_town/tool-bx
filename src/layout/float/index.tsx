import {
  ArrowLeftOutlined,
  HomeOutlined,
  SyncOutlined,
} from "@ant-design/icons";
import { FloatButton } from "antd";
import { useNavigate } from "react-router-dom";

/**
 * 右小角悬浮图标
 */
export function LayoutFloat() {
  const navigate = useNavigate();
  return (
    <>
      <FloatButton.Group shape="square" style={{ right: 0 }}>
        <FloatButton
          tooltip={<div>首页</div>}
          icon={<HomeOutlined />}
          onClick={() => navigate("/")}
        />
        <FloatButton
          tooltip={<div>返回</div>}
          icon={<ArrowLeftOutlined />}
          onClick={() => navigate(-1)}
        />
        <FloatButton
          tooltip={<div>刷新</div>}
          icon={<SyncOutlined />}
          onClick={() => location.reload()}
        />
        {/* <FloatButton.BackTop visibilityHeight={0} /> */}
      </FloatButton.Group>
    </>
  );
}
