import { TauriEvent, UnlistenFn, listen } from "@tauri-apps/api/event";
import { message } from "antd";
import { useEffect } from "react";
import { fileDropEmit } from "../utils/event";

/**
 * 监听fileDrop事件
 */
export function useFileDrop() {
  let unFileDrop: UnlistenFn;
  let unFileDropCancelled: UnlistenFn;
  let unFileDropHover: UnlistenFn;

  let ignore = false;

  useEffect(() => {
    if (ignore) return;

    (async () => {
      try {
        unFileDropHover = await listen<string[] | null>(
          TauriEvent.WINDOW_FILE_DROP_HOVER,
          () => {}
        );
      } catch (e) {
        console.error("File drop hover fail", e);
      }
    })();

    (async () => {
      try {
        unFileDropCancelled = await listen<string[] | null>(
          TauriEvent.WINDOW_FILE_DROP_CANCELLED,
          () => {
            message.info("取消文件拖入");
          }
        );
      } catch (e) {
        console.error("File drop cancel fail.", e);
      }
    })();

    (async () => {
      try {
        unFileDrop = await listen<string[] | null>(
          TauriEvent.WINDOW_FILE_DROP,
          (params) => {
            fileDropEmit(params);
            message.success("文件拖入完成");
          }
        );
      } catch (e) {
        console.error("File drop fail.", e);
      }
    })();

    return () => {
      ignore = true;
      unFileDrop?.();
      unFileDropCancelled?.();
      unFileDropHover?.();
    };
  }, []);
}
