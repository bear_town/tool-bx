import { useContext } from "react";
import { EnvContext } from "../../context";

export function Env() {
  const env = useContext(EnvContext);
  const {
    appName,
    appVersion,
    tauriVersion,
    osKernel,
    osPlatform,
    osVersion,
    arch,
  } = env;

  return (
    <div>
      <div>
        <div>appName: </div>
        <div>{appName}</div>
      </div>
      <div>
        <div>appVersion: </div>
        <div>{appVersion}</div>
      </div>
      <div>
        <div>tauriVersion: </div>
        <div>{tauriVersion}</div>
      </div>
      <div>
        <div>osPlatform: </div>
        <div>{osPlatform}</div>
      </div>
      <div>
        <div>osVersion: </div>
        <div>{osVersion}</div>
      </div>
      <div>
        <div>osKernel: </div>
        <div>{osKernel}</div>
      </div>
      <div>
        <div>arch: </div>
        <div>{arch}</div>
      </div>
    </div>
  );
}
