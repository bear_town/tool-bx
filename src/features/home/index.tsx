import { useCallback, useEffect, useState } from "react";
import { fetch } from "../../utils/fetch";
import { Account, pathGetAllAccounts } from "../../api";
import { Button, Card, message } from "antd";

import styles from "./index.module.less";
import { fileDropCb } from "../../utils/event";
import { fs } from "@tauri-apps/api";

export function Home() {
  const [accounts, setAccounts] = useState<Account[]>([]);

  const [imgUrl, setImgUrl] = useState("");

  // 为了保证执行一次，有点拉跨
  let ignore = false;
  useEffect(() => {
    if (!ignore) {
      fileDropCb(async (ppp) => {
        const { payload } = ppp;
        if (!payload?.[0]) return;
        const fileBinary = await fs.readBinaryFile(payload?.[0] as string);
        const imgUrl = URL.createObjectURL(new Blob([fileBinary]));
        setImgUrl(imgUrl);
      });
      (async () => {
        try {
          const res = await fetch<Account[]>({
            url: pathGetAllAccounts,
          });
          const { data = [] } = res;
          setAccounts(data);
        } catch (e) {
          message.error((e as Error).message);
        }
      })();
    }

    return () => {
      ignore = true;
    };
  }, []);

  const handleClick = useCallback(() => {
    message.info("click button");
  }, []);

  return (
    <div className={styles.home}>
      <div className={styles.accountsWrap}>
        {accounts.map((it) => {
          return (
            <Card
              key={it.id}
              title="平台信息"
              bordered={false}
              style={{ width: 300 }}
              className={styles.accountCard}
            >
              <p>id: {it.id}</p>
              <p>name: {it.name}</p>
              <p>role: {it.role}</p>
            </Card>
          );
        })}
      </div>
      <div>{imgUrl && <img src={imgUrl} alt="" />}</div>

      <Button type="primary" onClick={handleClick}>
        点击触发message
      </Button>
    </div>
  );
}
