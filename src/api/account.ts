import { getApiPath } from "./base";

/** 账户状态 */
export enum AccountStatus {
  /** 正常 */
  Normal = 1,
  /** 禁用 */
  Disable = 2,
  /** 注销 */
  Logout = 3,
}

/** 账户角色 */
export enum AccountRole {
  /** 管理员： 所有权限 */
  Admin = 1,
  /** 普通： 一般权限 */
  Ordinary = 2,
  /** 游客：无权限 */
  Visitor = 3,
}

/** 账号在线状态 */
export enum OnlineStatus {
  /** 在线 */
  Online = 1,
  /** 离线 */
  Offline = 2,
  /** 未知 */
  Unknown = 3,
}

/** 账号 */
export interface Account {
  /** 账号名称：默认游客111 */
  name: string;
  /** 账号ID：需要雪花算法 */
  id?: string;
  /** 密码：默认一个 */
  password: string;
  /** 账号状态，默认正常 */
  status: AccountStatus;
  /** 角色，默认游客 */
  role: AccountRole;
  /** 创建时间 */
  createTime: string;
  /** 修改时间 */
  modifyTime?: string;
  /** 在线状态：默认未知 */
  onlineStatus: OnlineStatus;
  /** 绑定的用户ID， 可以查询到用户信息 */
  bindUserId?: string;
}

/** 获取所有的账户 */
export const pathGetAllAccounts = getApiPath("/account/get-all");
