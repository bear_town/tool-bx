/**
 * 统一对外提供网络请求接口
 */

const HOST = "127.0.0.1";
const PORT = "3001";
const DOMAIN = "www.xiongzhen.top";
const PROTOCOL = "http://";

/** 获取完整的请求路径 */
export function getApiPath(path: string, isIpPort = true) {
  if (path.indexOf("://") < 0) {
    return `${PROTOCOL}${isIpPort ? `${HOST}:${PORT}` : DOMAIN}${path}`;
  }
  return path;
}
