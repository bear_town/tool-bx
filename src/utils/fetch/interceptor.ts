import { ErrorCode, HttpCode } from "./error_code";
import { PreResponse, RequestRarams } from "./type";

/**
 * 请求拦截器
 */
export function requestInterceptor(
  requestParams: RequestRarams
): RequestRarams {
  return requestParams;
}

/**
 * 响应拦截器
 */

export function responseInterceptor<T>(response: PreResponse<T>) {
  const { data: body, status, errorMsg } = response;
  const { code, msg, data } = body ?? {};
  if (status === HttpCode.Success) {
    if (code === ErrorCode.Ok) {
      return {
        code,
        msg,
        data,
      };
    }
    // 业务抛出错误
    console.error("业务错误码错误", msg);
    throw new Error(msg);
  }

  console.error("响应状态码错误", errorMsg);
  throw new Error(errorMsg);
}
