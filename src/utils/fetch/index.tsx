import {
  Body,
  Response as ResponseTauri,
  fetch as fetchTauriNet,
} from "@tauri-apps/api/http";
import { requestInterceptor, responseInterceptor } from "./interceptor";
import { PreResponse, RequestRarams } from "./type";
import { HttpCode } from "./error_code";

const qs = (query?: Record<string, any>) => {
  if (!query) return;
  const len = Object.keys(query).length;
  Object.entries(query).map(([key, value], idx) => {
    const isNeedAnd = len - 1 !== idx;
    return `${key}=${value}${isNeedAnd ? "&" : ""}`;
  });
};

const fetchWeb = (params: RequestRarams) => {
  const { url, method = "GET", query, body, header, timeout } = params;
  let [Url, hash] = url.split("#");
  const realUrl = `${Url}${query ? "?" + qs(query) : ""}${
    hash ? "#" + hash : ""
  }`;

  // 超过时间取消请求
  const controller = new AbortController();
  setTimeout(() => {
    controller.abort();
  }, timeout);

  return window.fetch(realUrl, {
    method,
    body: body as BodyInit,
    headers: header as HeadersInit,
    signal: controller.signal,
  });
};

const fetchTauri = (params: RequestRarams) => {
  const { url, method = "GET", query, body, header, timeout } = params;
  const realBody = body && Body.json(body);
  return fetchTauriNet(url, {
    method,
    query,
    body: realBody,
    headers: header,
    timeout,
  });
};

/**
 * WebFetch响应的处理
 */
async function fetchResWeb<T>(response: Response): Promise<PreResponse<T>> {
  const { status, statusText, url, headers } = response;
  let newData;
  let errMsg = "Ok";
  try {
    newData = await response.json();
    if (status !== HttpCode.Success) {
      errMsg = newData?.error + "," + newData?.message;
      newData = undefined;
    }
  } catch (e) {
    errMsg = "解析响应失败";
    throw new Error(errMsg);
  }

  return {
    data: newData,
    headers,
    url,
    status,
    errorMsg: statusText,
  };
}

/**
 * Tauri响应的处理
 */
async function fetchResTauri<T>(
  response: ResponseTauri<any>
): Promise<PreResponse<T>> {
  const { data, headers, status, url } = response;
  let newData = data;
  let errMsg = "Ok";
  if (status !== HttpCode.Success) {
    errMsg = data?.error + "," + data?.message;
    newData = undefined;
  }
  return {
    data: newData,
    headers,
    url,
    status,
    errorMsg: errMsg,
  };
}

/**
 * 请求工具函数
 * 进行拦截器的注册
 */
export async function fetch<T>(params: RequestRarams, useWeb = false) {
  // 进行请求的拦截器处理
  const requestParams = requestInterceptor(params);
  // 默认超时时间时10s
  const { timeout = 10_000 } = requestParams;

  // 进行适配器的选择
  let fetchFn;
  let fetchRes;
  if (useWeb) {
    fetchFn = fetchWeb;
    fetchRes = fetchResWeb;
  } else {
    fetchFn = fetchTauri;
    fetchRes = fetchResTauri;
  }

  let res;
  // 单独捕获请求
  try {
    // 所有正常的请求错误码走这个
    res = await fetchFn({ timeout, ...requestParams });
    // @ts-ignore 取消校验
    res = await fetchRes<T>(res);
  } catch (err) {
    // 网络错误才会走这个
    console.error("网络请求失败", err);
    // 抛出异常，使用catch捕获
    throw err;
  }

  // 进行响应的拦截处理
  const response = responseInterceptor<T>(res);
  return response;
}
