/**
 * 网络错误码
 */
export enum HttpCode {
  Success = 200,
}

/**
 * 自定义错误码
 */
export enum ErrorCode {
  /** 成功 */
  Ok = 0,
}

/**
 * 错误码信息映射
 */
export const ErrMsg: Record<ErrorCode, string> = {
  /** 请求成功 */
  [ErrorCode.Ok]: "请求成功",
};

/**
 * 通过错误码获取错误信息
 */
export function getErrMsg(errorCode: ErrorCode) {
  return ErrMsg[errorCode];
}

/** 请求时否OK */
export function isOk(errorCode: ErrorCode) {
  return errorCode === ErrorCode.Ok;
}
