import { HttpVerb } from "@tauri-apps/api/http";
import { ErrorCode } from "./error_code";

export type HttpMethod = HttpVerb;

/** 请求参数 */
export interface RequestRarams {
  url: string;
  /** 请求方法，默认是GET */
  method?: HttpMethod;
  /** 需要转化为String */
  query?: Record<string, any>;
  /** payload */
  body?: Record<string, any>;
  /** 超时时间 */
  timeout?: number;
  /** 请求头 */
  header?: Record<string, any>;
}

export interface PreResponse<T> {
  url: string;
  headers: Record<string, any>;
  status: number;
  data?: Response<T>;
  errorMsg: string;
}

/** 响应 */
export interface Response<T> {
  code: ErrorCode;
  msg?: string;
  data?: T;
}
