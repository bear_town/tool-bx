import { invoke } from "@tauri-apps/api/tauri";

/** 关闭用于展示内容的的窗口 */
export async function native_close_splashscreen() {
  const nativeCloseSplashScreen = "close_splashscreen";
  return invoke(nativeCloseSplashScreen);
}
