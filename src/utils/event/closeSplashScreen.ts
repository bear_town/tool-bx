import { native_close_splashscreen } from "../native";
/**
 * 关闭首屏页面，JS是单线程，不需要担心死锁问题
 */
export function closeSplashScreen() {
  const TIMEOUT_TIME = 3000;

  // DOMContentLoaded 是否完成
  let isLoaded = false;
  // 延时器是否完成
  let isTimeout = false;

  const timer = setTimeout(() => {
    if (isLoaded) {
      // 如果DOMContent已经加载完成，关闭窗口
      native_close_splashscreen();
    } else {
      // 如果DOMContent没有加载完成，等待load结束后在展示
      isTimeout = true;
    }
  }, TIMEOUT_TIME);

  const closeHandler = () => {
    if (isTimeout) {
      native_close_splashscreen();
    } else {
      isLoaded = true;
    }
  };
  // 注册事件监听器
  document.addEventListener("DOMContentLoaded", closeHandler);

  // 清除副作用
  const cancel = () => {
    // 去除延时器
    clearTimeout(timer);
    // 去除事件监听
    document.removeEventListener("DOMContentLoaded", closeHandler);
    // 重置数据
    isTimeout = false;
    isLoaded = false;
  };
  return cancel;
}
