import { event } from "@tauri-apps/api";
import { EventCallback } from "@tauri-apps/api/event";

export const FileDropEventName = "file-drop";

type FileDropEvent = Parameters<EventCallback<string[] | null>>[0];

/** 文件拖入的事件回调函数 */
export function fileDropCb(cb: EventCallback<string[] | null>) {
  // 监听事件
  event.listen(FileDropEventName, (eventPayload: FileDropEvent) => {
    cb?.(eventPayload);
  });
}

export function fileDropEmit(payload: FileDropEvent) {
  // 回调事件,这里会包裹一次Event
  const { payload: filesPath } = payload;
  event.emit(FileDropEventName, filesPath);
}
