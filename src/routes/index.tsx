import { createBrowserRouter } from "react-router-dom";
import HomePage from "../pages/home";
import NotFoundPage from "../pages/404";
import { MainLayout } from "../layout";
import { ReactNode, Suspense, lazy } from "react";

function lazyLoad(children: ReactNode) {
  return <Suspense fallback={<>Loading...</>}>{children}</Suspense>;
}

/** 关于页面 */
const About = lazy(() => import("../pages/about"));
/** Env页面 */
const Env = lazy(() => import("../pages/env"));

export const router = createBrowserRouter([
  {
    path: "/",
    element: <MainLayout />, // 能够获取到路由的上下文
    children: [
      {
        index: true,
        element: <HomePage />,
      },
      {
        path: "/about",
        element: lazyLoad(<About />),
      },
      {
        path: "/env",
        element: lazyLoad(<Env />),
      },
      {
        path: "/404",
        element: <NotFoundPage />,
      },
    ],
  },
]);
